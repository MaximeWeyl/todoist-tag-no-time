import click
from flask import Flask, request
import json
import requests
import todoist
import logging

app = Flask(__name__)


def get_users():
    try:
        with open("users.json", "r") as f:
            users = json.load(f)
    except Exception:
        users = {}  
    return users


def save_users(users):
    with open("users.json", "w") as f:
        json.dump(users, f)


@app.route("/")
def hello():
    return "Nothing to see here"


@app.route("/link")
def register_link():
    link = ("https://todoist.com/oauth/authorize?client_id={client_id}&"
            "scope=data:read_write&state={state}".format(
                client_id=CLIENT_ID, state=STATE))
    return "<a href='{}'>LINK</a>".format(link)


@app.route("/authorize", methods=["POST", "GET"])
def authorize():
    state = request.args["state"]
    code = request.args["code"]

    print(state, STATE)
    assert state == STATE

    data = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET, 
        "code": code,
    }
    r = requests.post("https://todoist.com/oauth/access_token", data=data)
    access_token = json.loads(r.content)['access_token']
    print(access_token)

    api = todoist.TodoistAPI(access_token)
    api.sync()
    user = api.user.get()
    user_id = user["id"]

    users = get_users()
    users[user_id] = access_token
    save_users(users)

    return "Welcome to user {} with token {}".format(
        user_id, access_token)


@app.route("/hook", methods=["POST", "GET"])
def hook():
    with open("hook_output.json", "wb") as f:
        f.write(request.data)

    data = json.loads(request.data)
    assert data["event_name"].startswith("item:")

    user_id = data["user_id"]
    users = get_users()
    access_token = users[str(user_id)]
    item_id = data["event_data"]["id"]

    update_labels(access_token, [item_id])
    return "OK"


def update_labels(access_token, item_ids):
    api = todoist.TodoistAPI(access_token)
    api.sync()

    labels = dict(
        (l.data["name"], l.data["id"])
        for l in api.state["labels"]
    )
    assert LABEL_NAME in labels
    label_id = labels[LABEL_NAME]

    for item_id in item_ids:
        item = api.items.get_by_id(item_id)
        update_item_label(item, label_id)
    item.api.commit()



def update_item_label(item, label_id):
    labels = item["labels"]
    all_day = item["all_day"]

    update_label = False
    if not all_day and label_id in labels:
        print("Removing the label")
        labels.remove(label_id)
        update_label = True
    if all_day and label_id not in labels:
        print("Adding the label")
        labels.append(label_id)
        update_label = True
    if update_label:
        item.update(labels=labels)


@app.route("/treat_all", methods=["POST", "GET"])
def treat_all(access_token=None):
    if access_token is None:
        access_token = request.args["access_token"]

    api = todoist.TodoistAPI(access_token)
    api.sync()
    items = api.state["items"]

    item_ids = [item["id"] for item in items]
    update_labels(access_token, item_ids)
    print("Treat all : finished")

    return str(item_ids)


@click.command()
@click.argument("client_id")
@click.argument("client_secret")
@click.argument("label_name")
@click.argument("state")
def main(client_id, client_secret, label_name, state):
    logging.warn("Running with args : {}".format(locals()))
    globals()["CLIENT_ID"] = client_id
    globals()["CLIENT_SECRET"] = client_secret
    globals()["LABEL_NAME"] = label_name
    globals()["STATE"] = state

    port = 5000
    host = "0.0.0.0"
    logging.warn("Running on host {} and port {}".format(host, port))
    app.run(host=host, port=port)


if __name__ == '__main__':
    main()